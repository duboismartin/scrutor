var mongoose = require('mongoose');
var express = require('express');
var router = express.Router();
var crypto = require('crypto');

var userHandlers = require('../../controllers/userController');
var slackHandlers = require('../../controllers/slackController');

var slackSignatureCheck = function(req, res, next){
    var request_body = JSON.stringify(req.body);

    var timestamp = req.headers['x-slack-request-timestamp'];
    var sig_basestring = 'v0:'+timestamp+':'+request_body;

    var hmac = crypto.createHmac('sha256', process.env.SLACK_SIGNING_KEY);
    hmac.update(sig_basestring);

    var my_signature = "v0=" + hmac.digest('hex');

    if(my_signature == req.headers['x-slack-signature']){
        next();
    }else{
        console.log("Error slack verification");
        console.log(my_signature);
        console.log(req.headers['x-slack-signature']);
        res.status(401).json({"error": "slack_signature_check_fail"});
    }
};

var middleUrlVerificationCheck = function(req, res, next){
    if(req.body['type'] == "url_verification"){
        res.json({challenge: req.body.challenge});
    }else{
        next();
    }
};

router.use('/eventReceiver', slackSignatureCheck);
router.use('/eventReceiver',middleUrlVerificationCheck);

/* GET users listing. */
router.get('/event/message/:userName',
    userHandlers.loginRequired,
    slackHandlers.findByName
);

router.post('/eventReceiver',
    slackHandlers.receiver
);

module.exports = router;
