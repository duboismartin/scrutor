var express = require('express');
var router = express.Router();
var logReportHandlers = require('../../controllers/logReportController');
var authRequired = require('../../middleware/authRequire');

/* GET users listing. */
router.get('/', authRequired, logReportHandlers.all);

router.post('/', authRequired, logReportHandlers.create);

router.get('/:id', authRequired, logReportHandlers.findByID);

router.delete('/:id', authRequired, logReportHandlers.deleteByID);

router.put('/:id', authRequired, logReportHandlers.updateById);

module.exports = router;
