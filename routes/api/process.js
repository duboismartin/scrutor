var express = require('express');
var router = express.Router();
var processHandlers = require('../../controllers/processController');
var authRequired = require('../../middleware/authRequire');

/* GET users listing. */
router.get('/', authRequired, processHandlers.all);

router.post('/', authRequired, processHandlers.create);

router.get('/:id', authRequired, processHandlers.findByID);

router.delete('/:id', authRequired, processHandlers.deleteByID);

router.put('/:id', authRequired, processHandlers.updateById);

router.get('/:id/logReports', authRequired, processHandlers.getAllReports);

router.get('/:id/logReports/byDate/:date', authRequired, processHandlers.getAllReportsByDay);

router.get('/:id/logReports/byDate/', authRequired, processHandlers.getAllReportsByDay);

module.exports = router;
