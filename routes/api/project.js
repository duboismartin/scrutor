var express = require('express');
var router = express.Router();
var projectHandlers = require('../../controllers/projectController');
var authRequired = require('../../middleware/authRequire');

/* GET users listing. */
router.get('/', authRequired, projectHandlers.all);

router.post('/', authRequired, projectHandlers.create);

router.get('/:id', authRequired, projectHandlers.findByID);

router.delete('/:id', authRequired, projectHandlers.deleteByID);

router.put('/:id', authRequired, projectHandlers.updateById);

router.get('/:id/processes', authRequired, projectHandlers.getAllProcesses);

module.exports = router;
