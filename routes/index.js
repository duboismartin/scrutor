var express = require('express');
var router = express.Router();
var axios = require('axios');
var mongoose = require('mongoose');
var slackEvent = mongoose.model('slackEvent');


var myLogger = function(req, res, next){
  req.requestTime = Date.now();
  next();
};

router.use(myLogger);

var retreivedUserName = function(userID){
    axios.get("https://slack.com/api/users.info?token="+process.env.SLACK_OAUTH+"&user="+userID)
        .then(response => {
            console.log(response.data.user.real_name);
            var result = slackEvent
                .updateMany(
                    { "event_details.user": userID },
                    { $set: { "event_details.userName": response.data.user.real_name } }
                ).then(response => {
                    console.log(response)
                }).catch(err => {
                    console.log(err);
                    console.error(err);
                });

            console.log(result);
        })
        .catch(error => {
            console.log(error);
        });
};

/* GET home page. */
router.get('/', function(req, res, next) {
    retreivedUserName("UJTD3B94M");
    res.render('index', { title: 'Express '+ req.requestTime+'' });
});

router.post('/stock', function(req, res, next) {
    console.log(req.body);
    res.send({status: "ok", body_received: req.body});
});

module.exports = router;
