var mongoose = require('mongoose');

var slackEvent = mongoose.model('slackEvent');

exports.findByName = function(req, res) {
    slackEvent.find({"event_details.type": "message"})
        .where("event_details.userName")
        .equals(req.params.userName)
        .exec((err, slackMessages) => {
            if(err){
                res.send({
                    message: 'Internal Error',
                })
            }

            if(!slackMessages){
                res.status(404).send({
                    message: 'Message does not exist.',
                });
            }else{
                res.send({
                    message: 'Message retrieved successfully by userID',
                    slackMessages,
                });
            }

        });
};

exports.receiver = function(req, res) {
    console.log("event received");
    console.log(req.body);

    var event_data = {
        event_type: req.body.event.type,
        event_details: req.body.event,
        event_time: req.body.event_time,
    };

    event_data.event_details["userName"] = "";

    var event = new slackEvent(event_data);
    event.save();
    res.json({
        event
    });
};
