var mongoose = require('mongoose');

var LogReport = mongoose.model('LogReport');

var validator = require('validator');

var axios = require("axios");
var reportSlack = function(log_report_data){
    var Message = "Erreur dans le script: "+log_report_data.processName+"\nThrowDate: "+log_report_data.throwDate+"\nDetails: "+log_report_data.details;
    axios({
        method: "POST",
        url: "https://slack.com/api/chat.postMessage",
        data: {
            "icon_url": "https://image.noelshack.com/fichiers/2019/19/5/1557501247-zoidberg2.png",
            "username": "Scrutor",
            "link_names": true,
            "channel": "test",
            "attachments": [{
                "color": "danger",
                "title": log_report_data.projectName,
                "title_link": "https://vps.edt.ovh/api-docs/",
                "text": Message,
                "footer": "Scrutor API",
                "footer_icon": "https://image.noelshack.com/fichiers/2019/19/5/1557501247-zoidberg2.png"
            }]
        },
        headers: {
            'Authorization': 'Bearer ' + process.env.SLACK_OAUTH,
            'Content-Type': 'application/json'
        }
    }).then(function(response){
        console.log("SLACK GOOD");
    }).catch(function(error){
        console.log("SLACK BAD");
        console.log(error);
    })
};

exports.create = function(req, res){

    if( validator.isEmpty(req.body.processName+'') ){
        return res.status(400).json({
            message: "Invalid processName"
        });
    }
    if( validator.isEmpty(req.body.logType+'') ){
        return res.status(400).json({
            message: "Invalid logType"
        });
    }
    if( req.body.error != undefined && !validator.isEmpty(req.body.error+'') && !validator.isNumeric(req.body.error+'') ){
        return res.status(400).json({
            message: "Error field specified but invalid, must be number"
        });
    }
    if( validator.isEmpty(req.body.projectName+'' )){
        return res.status(400).json({
            message: "Invalid projectName"
        });
    }

    var log_report_data = {
        processName: req.body.processName,
        projectName: req.body.projectName,
        logType: req.body.logType,
        throwDate: req.body.throwDate,
        details: req.body.details,
        error: req.body.error
    };

    if(log_report_data.error > 0){
        reportSlack(log_report_data);
    }

    var logReport = new LogReport(log_report_data);
    logReport.save(function (err, logReport) {
        if(err){
            return res.status(500).send({
                message: err
            });
        }else{
            return res.json({
                success: true,
                logReport
            });
        }
    });
};

exports.deleteByID = function(req, res){

    if( !validator.isMongoId(req.params.id+'') ){
        return res.status(400).json({
            message: "Specified id not a valid mongoDB id object"
        });
    }

    LogReport.findByIdAndRemove(
        req.params.id,
        function(err, logReport){
            if(err) throw err;
            if(logReport){
                return res.json({
                    success: true,
                    message: "LogReport successfully deleted"
                })
            }else{
                return res.status(404).json({
                    message: "LogReport not found"
                })
            }
        });
};

exports.findByID = function(req, res){

    if( !validator.isMongoId(req.params.id+'') ){
        return res.status(400).json({
            message: "Specified id not a valid mongoDB id object"
        });
    }

    LogReport.findOne({
        _id: req.params.id
    }, function(err, logReport){
        if(err) throw err;
        if(!logReport){
            return res.status(404).json({
                message: "LogReport not found"
            });
        }else if(logReport){
            return res.json({
                success: true,
                logReport
            });
        }
    });
};

exports.updateById = function(req, res){

    if( !validator.isMongoId(req.params.id+'') ){
        return res.status(400).json({
            message: "Specified id not a valid mongoDB id object"
        });
    }
    if( validator.isEmpty(req.body+'') ){
        return res.status(400).json({
            message: "No data to update"
        });
    }

    LogReport.updateOne({
        _id: req.params.id
    },{
        processName: req.body.processName,
        projectName: req.body.projectName,
        logType: req.body.logType,
        throwDate: req.body.throwDate,
        details: req.body.details,
        error: req.body.error
    }, function(err, logReport){
        if(err) throw err;
        if(logReport.nModified == 0){
            return res.status(404).json({
                message: "LogReport not found"
            });
        }else{
            return res.json({
                success: true,
                logReport
            })
        }
    });
};

exports.all = function(req, res){
    LogReport.find({

        },
        function(err, logReports){
            if(err) throw err;
            if(!logReports || logReports.length == 0){
                return res.status(404).json({
                    message: "No LogReports founds"
                });
            }else{
                return res.json({
                    success: true,
                    logReports
                });
            }
        });
};

exports.findByProcess = function(req, res){

    if( validator.isEmpty(req.params.processName+'') ){
        return res.status(400).json({
            succes: false,
            message: "Bad process name"
        })
    }

    LogReport.find({
            processName: req.params.processName
        },
        function(err, logReports){
            if(err) throw err;
            if(!logReports || logReports.length == 0){
                return res.status(404).json({
                    message: "No logReports for this process"
                });
            }else{
                return res.json({
                    success: true,
                    logReports
                });
            }
        });
};
