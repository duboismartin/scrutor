var mongoose = require('mongoose');

var Process = mongoose.model('Process');
var LogReport = mongoose.model('LogReport')

var validator = require('validator');

exports.create = function(req, res){

    if( validator.isEmpty(req.body.processName+'') ){
        return res.status(400).json({
            message: "Invalid processName"
        });
    }
    if( validator.isEmpty(req.body.projectName+'') ){
        return res.status(400).json({
            message: "Invalid projectName"
        });
    }
    if( validator.isEmpty(req.body.expectedTime+'') ){
        return res.status(400).json({
            message: "Invalid expectedTime"
        });
    }


    var process_data = {
        processName: req.body.processName,
        projectName: req.body.projectName,
        description: req.body.description,
        expectedTime: req.body.expectedTime
    };

    var process = new Process(process_data);
    process.save(function(err, process){
        if(err){
            return res.status(400).send({
                message: err
            });
        }else{
            return res.json({
                process
            });
        }
    });
};

exports.deleteByID = function(req, res){

    if( !validator.isMongoId(req.params.id+'') ){
        return res.status(400).json({
            message: "Specified id not a valid mongoDB id object"
        });
    }

    Process.findByIdAndRemove(
        req.params.id,
        function(err, process){
            if(err) throw err;
            if(process){
                return res.json({
                    message: "Process deleted"
                });
            }else{
                return res.status(404).json({
                    message: 'Process not found'
                });
            }
        });
};

exports.findByID = function(req, res){

    if( !validator.isMongoId(req.params.id+'') ){
        return res.status(400).json({
            message: "Specified id not a valid mongoDB id object"
        });
    }

    Process.findOne({
        _id: req.params.id
    }, function(err, process){
        if(err) throw err;
        if(!process){
            return res.status(404).json({
                message: "Process not found"
            });
        }else if(process){
            return res.json({
                process
            });
        }
    });
};

exports.updateById = function(req, res){

    if( !validator.isMongoId(req.params.id+'') ){
        return res.status(400).json({
            message: "Specified id not a valid mongoDB id object"
        });
    }
    if( validator.isEmpty(req.body+'') ){
        return res.status(400).json({
            message: "No data to update"
        });
    }
    if( validator.isEmpty(req.body.expectedTime+'') ){
        return res.status(400).json({
            message: "Invalid expectedTime"
        });
    }

    Process.updateOne({
        _id: req.params.id
    },{
        processName: req.body.processName,
        projectName: req.body.projectName,
        description: req.body.processDescription,
        expectedTime: req.body.expectedTime
    }, function(err, process){
        if(err) throw err;
        if(process.nModified == 0){
            return res.status(404).json({
                message: "Process not found"
            });
        }else{
            return res.json({
                process
            })
        }
    });
};

exports.all = function(req, res){
    Process.find({

    },
        function(err, processes){
            if(err) throw err;
            if(!processes || processes.length == 0){
                return res.status(404).json({
                    message: "No process founds"
                });
            }else{
                return res.json({
                    processes
                });
            }
        });
};

exports.findByProject = function(req, res){

    if( validator.isEmpty(req.body.projectName+'') ){
        return res.status(400).json({
            message: "Invalid projectName"
        });
    }

    Process.find({
        projectName: req.params.projectName
    },
        function(err, processes){
            if(err) throw err;
            if(!processes || processes.length == 0){
                return res.status(404).json({
                    message: "No process for this project"
                });
            }else{
                return res.json({
                    processes
                });
            }
        });
};

exports.getAllReports = function(req, res){

    if( !validator.isMongoId(req.params.id+'') ){
        return res.status(400).json({
            message: "Specified id not a valid mongoDB id object"
        });
    }

    Process.findOne({
        _id: req.params.id
    }, function(err, process){
        if(err) throw err;
        if(!process){
            return res.status(404).json({
                message: "Process not found"
            });
        }else if(process){
            LogReport.find({
                    processName: process.processName,
                    projectName: process.projectName
                },
                function(err, logReports){
                    if(err) throw err;
                    if(!logReports || logReports.length == 0){
                        return res.status(404).json({
                            message: "No logReports for this process"
                        });
                    }else{
                        return res.json({
                            logReports
                        });
                    }
                });
        }
    });
}

exports.getAllReportsByDay = function(req, res){

    if( !validator.isMongoId(req.params.id+'') ){
        return res.status(400).json({
            message: "Specified id not a valid mongoDB id object"
        });
    }

    if( req.params.date === undefined || validator.isEmpty(req.params.date+'') ){
        var dateNow = new Date(Date.now());
        req.params.date = dateNow.getFullYear()+"-"+( dateNow.getMonth().toString().length == 2 ? dateNow.getMonth() : '0' + dateNow.getMonth() )+"-"+( dateNow.getDate().toString().length == 2 ? dateNow.getDate() : '0' + dateNow.getDate() );
    }

    if( req.params.date != undefined && !validator.isEmpty(req.params.date+'') && (!validator.isLength(req.params.date+'', { min: 8, max: 10 }) || validator.toDate(req.params.date+'') == null ) ){
        return res.status(400).json({
            message: "Specified date is not a valid date, expected format : 'yyyy-mm-dd'",
        });
    }

    Process.findOne({
        _id: req.params.id
    }, function(err, process){
        if(err) throw err;
        if(!process){
            return res.status(404).json({
                message: "Process not found"
            });
        }else if(process){
            LogReport.find({
                    processName: process.processName,
                    projectName: process.projectName,
                    throwDate: { $gte: validator.toDate(req.params.date), $lte: validator.toDate(req.params.date+"T23:59:59")}
                },
                function(err, logReports){
                    if(err) throw err;
                    if(!logReports || logReports.length == 0){
                        return res.status(404).json({
                            message: "No logReports for this process",
                            test: validator.toDate(req.params.date),
                            testtest: req.params.date
                        });
                    }else{
                        return res.json({
                            logReports
                        });
                    }
                });
        }
    });
}
