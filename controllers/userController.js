var mongoose = require('mongoose');
var jwt = require('jsonwebtoken');
var bcrypt = require('bcrypt');
var User = mongoose.model('User');
var validator = require('validator');

exports.register = function (req, res) {

    if(validator.isEmpty(req.body.fullName+'')){
        return res.status(400).json({
            message: "Invalid fullName"
        });
    }
    if(validator.isEmpty(req.body.email+'') ||  !validator.isEmail(req.body.email)){
        return res.status(400).json({
            message: "Invalid email"
        });
    }
    if( !validator.isLength(req.body.password+'', { min: 8, max: undefined }) ){
        return res.status(400).json({
            message: "Invalid password, minimum length: 8"
        });
    }

    var newUser = new User(req.body);
    newUser.hash_password = bcrypt.hashSync(req.body.password, 10);
    newUser.save(function(err, user){
        if (err){
            if(err.code == 11000){
                return res.status(409).json({
                    message: "Email already used"
                });
            }else{
                return res.status(500).json({
                    message: err
                });
            }
        }else {
            user.hash_password = undefined;
            return res.json({
                user
            });
        }
    });
};

exports.sign_in = function (req, res) {

    if(validator.isEmpty(req.body.email+'') ||  !validator.isEmail(req.body.email+'')){
        return res.status(400).json({
            message: "Invalid email"
        });
    }
    if( !validator.isLength(req.body.password+'', { min: 8, max: undefined }) ){
        return res.status(400).json({
            message: "Invalid password, minimum length: 8"
        });
    }

    User.findOne({
        email: req.body.email
    }, function(err, user) {
        if (err) throw err;
        if(!user){
            res.status(404).json({
                message: 'Authentication failed. User not found.'
            });
        }else if(user){
            if(!user.comparePassword(req.body.password)){
                res.status(401).json({
                    message: 'Authentication failed. Wrong Pasword.'
                });
            }else{
                return res.json({
                    token: jwt.sign({ email: user.email, fullName: user.fullName, _id: user._id}, process.env.JWT_SECRET),
                    user: { email: user.email, name: user.fullName, id: user._id }
                });
            }
        }
    });
};

exports.loginRequired = function (req, res, next) {
    if(req.user){
        next();
    }else{
        return res.status(401).json({
            message: 'Unauthorized user!',
            user: req.user
        });
    }
};
