var mongoose = require('mongoose');

var Project = mongoose.model('Project');
var Process = mongoose.model('Process');

var validator = require('validator');

exports.create = function(req, res){

    if( validator.isEmpty(req.body.projectName+'') ){
        return res.status(400).json({
            message: "Invalid projectName"
        });
    }

    var project_data = {
        projectName: req.body.projectName,
    };

    var project = new Project(project_data);
    project.save(function(err, project){
        if (err){
            return res.status(400).send({
                message: err
            });
        }else {
            return res.json({
                project
            });
        }
    });
};

exports.deleteByID = function(req, res){

    if( !validator.isMongoId(req.params.id+'') ){
        return res.status(400).json({
            message: "Specified id not a valid mongoDB id object"
        });
    }

    Project.findByIdAndRemove(
        req.params.id,
        function(err, project){
            if(err) throw err;
            if(project){
                return res.json({
                    message: "Project deleted"
                });
            }else{
                return res.status(404).json({
                    message: 'Project not found'
                });
            }
        });
};

exports.findByID = function(req, res){

    if( !validator.isMongoId(req.params.id+'') ){
        return res.status(400).json({
            message: "Specified id not a valid mongoDB id object"
        });
    }

    Project.findOne({
        _id: req.params.id
    }, function(err, project){
        if(err) throw err;
        if(!project){
            return res.status(404).json({
                message: "Project not found"
            });
        }else if(project){
            return res.json({
                project
            });
        }
    });
};

exports.updateById = function(req, res){

    if( !validator.isMongoId(req.params.id+'') ){
        return res.status(400).json({
            message: "Specified id not a valid mongoDB id object"
        });
    }

    if( validator.isEmpty(req.body.projectName+'') ){
        return res.status(400).json({
            message: "Invalid projectName"
        });
    }

    Project.updateOne({
        _id: req.params.id
    },{
        projectName: req.body.projectName
    }, function(err, project){
        if(err) throw err;
        if(project.nModified == 0){
            return res.status(404).json({
                message: "Project not found"
            });
        }else{
            return res.json({
                project
            })
        }
    });
};

exports.all = function(req, res){
    Project.find({

        },
        function(err, projects){
            if(err) throw err;
            if(!projects || projects.length == 0){
                return res.status(404).json({
                    message: "No projects founds"
                });
            }else{
                return res.json({
                    projects
                });
            }
        });
};

exports.getAllProcesses = function(req, res){
    if( !validator.isMongoId(req.params.id+'') ){
        return res.status(400).json({
            message: "Specified id not a valid mongoDB id object"
        });
    }

    Project.findOne({
        _id: req.params.id
    }, function(err, project){
        if(err) throw err;
        if(!project){
            return res.status(404).json({
                message: "Project not found"
            });
        }else if(project){
            Process.find({
                projectName: project.projectName
            }, function(err, processes){
                if(err) throw err;
                if(!processes || processes.length == 0){
                    return res.status(404).json({
                        message: "No processes founds"
                    });
                }else if(processes){
                    return res.json({
                        processes
                    });
                }
            });
        }
    });
};
