const worker = require('worker_threads');

class Scrutor{

    #websiteUrl = "";
    #callInterval = 0;
    #active = false;

    constructor(websiteUrl, callInterval){
        this.#websiteUrl = websiteUrl;
        this.#callInterval = callInterval;
    }

    start(){
        this.#active = true;
    }

    stop(){
        this.#active = false;
    }



}