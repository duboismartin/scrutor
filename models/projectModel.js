var mongoose = require('mongoose');
var Schema = mongoose.Schema;

/**
 * Project Schema
 **/
var ProjectSchema = new Schema({
    projectName: {
        type: String,
        trim: true,
        required: true,
        unique: true
    }
},{
    timestamps: true
});

mongoose.model('Project', ProjectSchema);
