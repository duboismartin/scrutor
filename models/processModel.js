var mongoose = require('mongoose');
var Schema = mongoose.Schema;

/**
 * process Report Schema
 **/
var ProcessSchema = new Schema({
    processName: {
        type: String,
        trim: true,
        required: true
    },
    projectName: {
        type: String,
        trim: true,
        required: true
    },
    description: {
        type: String,
        required: false
    },
    expectedTime: {
        type: Date,
        required: true
    }
},{
    timestamps: true
});

ProcessSchema.index({ processName: 1, projectName: 1 }, { unique: true });

mongoose.model('Process', ProcessSchema);
