var mongoose = require('mongoose');
var Schema = mongoose.Schema;

const slackEventSchema = new Schema({
    event_type: String,
    event_details: Object,
    date_received: { type: Date, default: Date.now },
    event_time: String
},{
    timestamps: true
});

mongoose.model('slackEvent', slackEventSchema);