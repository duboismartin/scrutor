var mongoose = require('mongoose');
var Schema = mongoose.Schema;

/**
 * Log Report Schema
 **/
var LogReportSchema = new Schema({
    processName: {
        type: String,
        trim: true,
        required: true
    },
    projectName: {
        type: String,
        trim: true,
        required: true
    },
    logType: {
        type: String,
        required: true
    },
    throwDate: {
        type: Date,
        required: false,
        default: Date.now()
    },
    details: {
        type: String,
        required: false
    },
    error: {
        type: Number,
        required: false,
        default: 0
    }
},{
    timestamps: true
});

mongoose.model('LogReport', LogReportSchema);
