var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var mongoose = require('mongoose');
var swaggerUi = require('swagger-ui-express');
var swaggerDocument = require('./swagger.json');
var cors = require('cors');


//MODELS
var User = require('./models/userModel');
var slackEventModel = require('./models/slackEventModel');

//LOGGER MODELS
var Project = require('./models/projectModel');
var Process = require('./models/processModel');
var LogReport = require('./models/logReportModel');

require('dotenv').config();

var mongoose_options = {
  useNewUrlParser: true,
  user: process.env.DB_USER,
  pass: process.env.DB_PASS
};

mongoose.connect(
    process.env.NODE_ENV == "production" ? process.env.DB_URL_PRODUCTION :
        (process.env.NODE_ENV == "development" ? process.env.DB_URL_DEVELOPMENT : "")
    , mongoose_options);

var db = mongoose.connection;
db.on('error', console.error.bind(console, 'Erreur lors de la connexion'));
db.once('open', function (){
  console.log("Connexion à la base OK");
});

//ROUTE
var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');

//API ROUTE
var slackRouter = require('./routes/api/slack');
var authRouter = require('./routes/api/auth');
var projectRouter = require('./routes/api/project');
var processRouter = require('./routes/api/process');
var logReportRouter = require('./routes/api/logReport');

var checkToken = require('./middleware/checkToken');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(cors());

//Middleware before routes
app.use(checkToken);

app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/api/slack', slackRouter);
app.use('/api/auth', authRouter);
app.use('/api/project', projectRouter);
app.use('/api/process', processRouter);
app.use('/api/logReport', logReportRouter);

//SWAGGER
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
