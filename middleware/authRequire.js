module.exports = function(req, res, next){
    if(req.user != undefined){
        next();
    }else{
        res.status(401).json({
            message: "Authentication required"
        })
    }
}
