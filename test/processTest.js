const mongoose = require('mongoose');
const chai = require('chai');
const should = chai.should();
const chaiHttp = require('chai-http');

const server = require('../app');
var Process = mongoose.model('Process');

chai.use(chaiHttp);

describe('Processes', () =>{
    beforeEach((done) => {
        Process.remove({}, () => {
            done();
        });
    });

    describe('/GET process', () => {
        it('should get all process items when no items are in database', (done) => {
            chai.request(server).get('/api/process').end((err, res) => {
                res.should.have.status(404);
                res.body.should.be.a('object');
                done();
            });
        });

        it('should get all processes items when there are two items in the database', (done) => {
            const randomProcessOne = new Process({
                processName: "random1",
                projectName: "random1"
            });
            const randomProcessTwo = new Process({
                processName: "random2",
                projectName: "random2",
                description: "random2"
            });

            randomProcessOne.save(() => {
                randomProcessTwo.save(() => {
                    chai.request(server).get('/api/process').end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.be.a('object');
                        res.body.processes[0].processName.should.be.eql(randomProcessOne.processName);
                        res.body.processes[0].projectName.should.be.eql(randomProcessOne.projectName);
                        res.body.processes[1].processName.should.be.eql(randomProcessTwo.processName);
                        res.body.processes[1].projectName.should.be.eql(randomProcessTwo.projectName);
                        res.body.processes[1].description.should.be.eql(randomProcessTwo.description);
                        done();
                    });
                });
            });
        });
    });

    describe('/POST process', () => {
        it('should not post a process if projectName is undefined', (done) => {
            const param = {
                projectName: '',
                processName: 'rdm'
            };
            chai.request(server).post('/api/process').send(param).end((err, res) => {
                res.should.have.status(400);
                res.body.should.be.a('object');
                done();
            });
        });


        it('should not post a process if processName is undefined', (done) => {
            const param = {
                projectName: 'rdm',
                processName: ''
            };
            chai.request(server).post('/api/process').send(param).end((err, res) => {
                res.should.have.status(400);
                res.body.should.be.a('object');
                done();
            });
        });
    });

    describe('/GET/:id process', () => {
        it('should not get a process because it does not exist', (done) => {
            chai.request(server).get('/api/process/' + mongoose.Types.ObjectId()).end((err, res) => {
                res.should.have.status(404);
                done();
            });
        });

        it('should get a process', (done) => {
            const randomProcessThree = new Process({
                projectName: 'random3',
                processName: 'random3'
            });
            randomProcessThree.save((err, savedProcess) => {
                chai.request(server).get('/api/process/' + savedProcess._id).end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.process["projectName"].should.be.eql(savedProcess.projectName);
                    res.body.process["processName"].should.be.eql(savedProcess.processName);
                    done();
                });
            });
        });
    });

    describe('/PUT process', () => {
        it('should not put a process because it does not exist', (done) => {
            const param = {
                projectName: "notRandom",
                processName: "notRandom"
            };
            chai.request(server).put('/api/process/' + mongoose.Types.ObjectId()).send(param).end((err, res) => {
                res.should.have.status(404);
                done();
            });
        });

        it('should put a process and update the name value', (done) => {
            const randomProcessFour = new Process({
                projectName: "random4",
                processName: "random4"
            });
            const param = {
                projectName: "newRandom4",
                processName: "newRandom4"
            };
            randomProcessFour.save((err, savedProcess) => {
                chai.request(server).put('/api/process/' + savedProcess._id).send(param).end((err, res) => {
                    res.should.have.status(200);
                    Process.find({}, (err, processes) => {
                        processes[0].processName.should.eql(param.processName);
                        processes[0].projectName.should.eql(param.projectName);
                        done();
                    });
                });
            });
        });
    });

    describe('/DELETE/:id process', () => {
        it('should not delete a process because it does not exist', (done) => {
            chai.request(server).delete('/api/process/' + mongoose.Types.ObjectId()).end((err, res) => {
                res.should.have.status(404);
                done();
            });
        });

        it('should delete a process', (done) => {
            const randomProcessFive = new Process({
                projectName: "newRandom5",
                processName: "newRandom5"
            });
            randomProcessFive.save((err, savedProcess) => {
                chai.request(server).delete('/api/process/' + savedProcess._id).end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    Process.find({}, (err, processes) => {
                        processes.length.should.be.eql(0);
                    });
                    done();
                });
            });
        });
    });

});
