const mongoose = require('mongoose');
const chai = require('chai');
const should = chai.should();
const chaiHttp = require('chai-http');

const server = require('../app');
var Project = mongoose.model('Project');

chai.use(chaiHttp);

describe('Projects', () =>{
    beforeEach((done) => {
        Project.remove({}, () => {
            done();
        });
    });

    describe('/GET project', () => {
        it('should get all projects items when no items are in database', (done) => {
            chai.request(server).get('/api/project').end((err, res) => {
                res.should.have.status(404);
                res.body.should.be.a('object');
                done();
            });
        });

        it('should get all project items when there are two items in the database', (done) => {
            const randomProjectOne = new Project({
                projectName: "random1"
            });
            const randomProjectTwo = new Project({
                projectName: "random2"
            });

            randomProjectOne.save(() => {
                randomProjectTwo.save(() => {
                    chai.request(server).get('/api/project').end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.be.a('object');
                        res.body.projects[0].projectName.should.be.eql(randomProjectOne.projectName);
                        res.body.projects[1].projectName.should.be.eql(randomProjectTwo.projectName);
                        done();
                    });
                });
            });
        });
    });

    describe('/POST project', () => {
        it('should not post a project if projectName is undefined', (done) => {
            const param = {
                projectName: ''
            };
            chai.request(server).post('/api/project').send(param).end((err, res) => {
                res.should.have.status(400);
                res.body.should.be.a('object');
                done();
            });
        });
    });

    describe('/GET/:id project', () => {
        it('should not get a project because it does not exist', (done) => {
            chai.request(server).get('/api/project/' + mongoose.Types.ObjectId()).end((err, res) => {
                res.should.have.status(404);
                done();
            });
        });

        it('should get a project', (done) => {
            const randomProjectThree = new Project({
                projectName: 'random3'
            });
            randomProjectThree.save((err, savedProject) => {
                chai.request(server).get('/api/project/' + savedProject._id).end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.project["projectName"].should.be.eql(savedProject.projectName);
                    done();
                });
            });
        });
    });

    describe('/PUT project', () => {
        it('should not put a project because it does not exist', (done) => {
            const param = {
                projectName: "notRandom"
            };
            chai.request(server).put('/api/project/' + mongoose.Types.ObjectId()).send(param).end((err, res) => {
                res.should.have.status(404);
                done();
            });
        });

        it('should put a project and update the name value', (done) => {
            const randomProjectFour = new Project({
                projectName: "random4"
            });
            const param = {
                projectName: "newRandom4"
            };
            randomProjectFour.save((err, savedProject) => {
                chai.request(server).put('/api/project/' + savedProject._id).send(param).end((err, res) => {
                    res.should.have.status(200);
                    Project.find({}, (err, projects) => {
                        projects[0].projectName.should.eql(param.projectName);
                        done();
                    });
                });
            });
        });
    });

    describe('/DELETE/:id project', () => {
        it('should not delete a project because it does not exist', (done) => {
            chai.request(server).delete('/api/project/' + mongoose.Types.ObjectId()).end((err, res) => {
                res.should.have.status(404);
                done();
            });
        });

        it('should delete a project', (done) => {
            const randomProjectFive = new Project({
                projectName: "newRandom5"
            });
            randomProjectFive.save((err, savedProject) => {
                chai.request(server).delete('/api/project/' + savedProject._id).end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    Project.find({}, (err, projects) => {
                        projects.length.should.be.eql(0);
                    });
                    done();
                });
            });
        });
    });

});
