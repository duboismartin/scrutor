const mongoose = require('mongoose');
const chai = require('chai');
const should = chai.should();
const chaiHttp = require('chai-http');

const server = require('../app');
var LogReport = mongoose.model('LogReport');

chai.use(chaiHttp);

describe('LogReports', () =>{
    beforeEach((done) => {
        LogReport.remove({}, () => {
            done();
        });
    });

    describe('/GET logReport', () => {
        it('should get all logReport items when no items are in database', (done) => {
            chai.request(server).get('/api/logReport').end((err, res) => {
                res.should.have.status(404);
                res.body.should.be.a('object');
                done();
            });
        });

        it('should get all logReports items when there are two items in the database', (done) => {
            const randomLogReportOne = new LogReport({
                processName: "random1",
                logType: "random1",
                error: false
            });
            const randomLogReportTwo = new LogReport({
                processName: "random2",
                logType: "random2",
                error: true
            });

            randomLogReportOne.save(() => {
                randomLogReportTwo.save(() => {
                    chai.request(server).get('/api/logReport').end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.be.a('object');
                        res.body.logReports[0].processName.should.be.eql(randomLogReportOne.processName);
                        res.body.logReports[0].logType.should.be.eql(randomLogReportOne.logType);
                        res.body.logReports[0].error.should.be.eql(randomLogReportOne.error);

                        res.body.logReports[1].processName.should.be.eql(randomLogReportTwo.processName);
                        res.body.logReports[1].logType.should.be.eql(randomLogReportTwo.logType);
                        res.body.logReports[1].error.should.be.eql(randomLogReportTwo.error);

                        done();
                    });
                });
            });
        });
    });

    describe('/POST logReport', () => {
        it('should not post a logReport if processName is undefined', (done) => {
            const param = {
                processName: "",
                logType: "param",
                error: false
            };
            chai.request(server).post('/api/logReport').send(param).end((err, res) => {
                res.should.have.status(400);
                res.body.should.be.a('object');
                done();
            });
        });


        it('should not post a logReport if logType is undefined', (done) => {
            const param = {
                processName: "param",
                logType: "",
                error: false
            };
            chai.request(server).post('/api/logReport').send(param).end((err, res) => {
                res.should.have.status(400);
                res.body.should.be.a('object');
                done();
            });
        });
    });

    describe('/GET/:id logReport', () => {
        it('should not get a logReport because it does not exist', (done) => {
            chai.request(server).get('/api/logReport/' + mongoose.Types.ObjectId()).end((err, res) => {
                res.should.have.status(404);
                done();
            });
        });

        it('should get a logReport', (done) => {
            const randomLogReportThree = new LogReport({
                processName: "random3",
                logType: "random3",
            });
            randomLogReportThree.save((err, savedLogReport) => {
                chai.request(server).get('/api/logReport/' + savedLogReport._id).end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.logReport["logType"].should.be.eql(savedLogReport.logType);
                    res.body.logReport["processName"].should.be.eql(savedLogReport.processName);
                    done();
                });
            });
        });
    });

    describe('/PUT logReport', () => {
        it('should not put a logReport because it does not exist', (done) => {
            const param = {
                processName: "notRandom",
                logType: "notRandom",
            };
            chai.request(server).put('/api/logReport/' + mongoose.Types.ObjectId()).send(param).end((err, res) => {
                res.should.have.status(404);
                done();
            });
        });

        it('should put a logReport and update the name value', (done) => {
            const randomLogReportFour = new LogReport({
                logType: "random4",
                processName: "random4"
            });
            const param = {
                logType: "newRandom4",
                processName: "newRandom4"
            };
            randomLogReportFour.save((err, savedLogReport) => {
                chai.request(server).put('/api/logReport/' + savedLogReport._id).send(param).end((err, res) => {
                    res.should.have.status(200);
                    LogReport.find({}, (err, logReports) => {
                        logReports[0].processName.should.eql(param.processName);
                        logReports[0].logType.should.eql(param.logType);
                        done();
                    });
                });
            });
        });
    });

    describe('/DELETE/:id logReport', () => {
        it('should not delete a logReport because it does not exist', (done) => {
            chai.request(server).delete('/api/logReport/' + mongoose.Types.ObjectId()).end((err, res) => {
                res.should.have.status(404);
                done();
            });
        });

        it('should delete a logReport', (done) => {
            const randomLogReportFive = new LogReport({
                logType: "newRandom5",
                processName: "newRandom5"
            });
            randomLogReportFive.save((err, savedLogReport) => {
                chai.request(server).delete('/api/logReport/' + savedLogReport._id).end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    LogReport.find({}, (err, logReports) => {
                        logReports.length.should.be.eql(0);
                    });
                    done();
                });
            });
        });
    });

});
